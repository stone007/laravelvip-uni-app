import request from '@/utils/request'

// api地址
const api = {
  todoCounts: 'order/todoCounts',
  list: '/user/order.html',
  detail: '/user/order/info',
  express: '/user/order/express.html',
  cancel: '/user/order/cancel.html',
  receipt: '/user/order/confirm',
  pay: '/checkout/pay.html'
}

// 当前用户待处理的订单数量
export function todoCounts(param, option) {
  return request.get(api.todoCounts, param, option)
}

// 我的订单列表
export function list(param, option) {
  return request.get(api.list, param, option)
}

// 订单详情
export function detail(id, param) {
  return request.get(api.detail, { id, ...param })
}

// 获取物流信息
export function express(id, param) {
  return request.get(api.express, { id, ...param })
}

// 取消订单
export function cancel(id, data) {
  return request.post(api.cancel, { id, ...data })
}

// 确认收货
export function receipt(id, data) {
  return request.post(api.receipt, { id, ...data })
}

// 立即支付
export function pay(id, payType, param) {
  return request.get(api.pay, { id, payType, ...param })
}
