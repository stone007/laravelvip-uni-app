import request from '@/utils/request'

// api地址
const api = {
  list: '/category.html'
}

// 页面数据
export function list() {
  return request.get(api.list)
}
