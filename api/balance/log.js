import request from '@/utils/request'

// api地址
const api = {
  list: '/user/capital-account.html'
}

// 余额账单明细
export const list = (param) => {
  return request.get(api.list, param)
}
