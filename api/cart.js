import request from '@/utils/request'

// api地址
const api = {
  list: '/cart.html',
  total: 'cart/total',
  add: '/cart/add.html',
  changeNumber: '/cart/change-number.html',
	quickBuy: '/cart/quick-buy.html',
  clear: 'cart/clear'
}

// 购物车列表
export const list = () => {
  return request.get(api.list, {}, { load: false })
}

// 购物车商品总数量
export const total = () => {
  return request.get(api.total, {}, { load: false })
}

// 加入购物车
export const add = (goods_id, sku_id, number) => {
  return request.post(api.add, { goods_id, sku_id, number })
}

// 更新购物车商品数量
export const changeNumber = (goods_id, sku_id, number) => {
  return request.post(api.changeNumber, { goods_id, sku_id, number }, { isPrompt: false })
}

// 立即购买
export const quickBuy = (goods_id, sku_id, number) => {
  return request.post(api.quickBuy, { goods_id, sku_id, number })
}

// 删除购物车中指定记录
export const clear = (cartIds = []) => {
  return request.post(api.clear, { cartIds })
}
